package com.example.faustdehng.myapplication

import android.graphics.Color
import android.graphics.Typeface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.TooltipCompat
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    val numberGuess = NumberGuess()

    /**
     * Create the main activity
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.btnGo.setOnClickListener  { this.showResult() }
        this.btnAns.setOnClickListener { this.txtResult.text = this.numberGuess.answer }
        this.btnNew.setOnClickListener {
            this.numberGuess.renew()
            this.txtResult.text = ""
            this.editGuess.setText("")
        }
    }

    fun clickBtnGo(view: View) {
        this.showResult()
    }


    /**
     * Show the checking result
     */
    fun showResult() {
        val result = numberGuess.checkABs(this.editGuess.text.toString())
        if(result.equals("4A0B")) {
            this.txtResult.setTextColor(Color.parseColor("#006400") )  //RGB code of Dark Green
            this.txtResult.typeface = Typeface.DEFAULT_BOLD
            this.txtResult.text = "Bingo !!"
        } else {
            this.txtResult.setTextColor(Color.RED)
            this.txtResult.typeface = Typeface.DEFAULT
            this.txtResult.text = result
        }
    }

}
