package com.example.faustdehng.myapplication

import java.util.*

class NumberGuess {
    /**
     * property, to get the answer string, such as "1234"
     */
    var answer: String
        get() { return this.answer }

    /**
     * constructor
     */
    init {
        this.answer = this.getInit()
    }

    /**
     * Get the (random) initial value for a new game
     *
     * 4 different number digits, leading zeros is allowed
     */
    private fun getInit() : String {
        var answerDigit: String
        do {
            val random = Random()
            val answer = random.nextInt(10000)
            val digits = answer.toString()
            answerDigit = "0".repeat(4 - digits.length)+digits
        } while(!isValid(answerDigit))
        return answerDigit
    }

    /**
     * reset the answer
     */
    public fun renew() {
        this.answer = this.getInit()
    }

    /**
     * check if there are same digits
     *
     * @return true for no same digits, 4 digits are different, false for length wrong or same digits
     */
    public fun isValid(answer: String): Boolean {
        if(answer.length == 4) {
            for(i in 0..2) {
                for(j in (i+1)..3) {
                    if(answer.get(i)==answer.get(j)) {
                        return false
                    }
                }
            }
            return true
        }
        return false
    }

    /**
     * Check the anser and the guess, return how many A or B
     *
     * @param   answer  the answer of the game
     * @param   guess   the new guess
     * @return  Pair of (As, Bs), or null for wrong input
     */
    public fun checkABs(guess: String): String? {
        if (this.answer.length != 4 || guess.length != 4) {
            return null
        } else {
            var a: Int = 0
            var b: Int = 0
            for (i in 0..3) {
                for (j in 0..3) {
                    if (this.answer.get(i) == guess.get(j)) {
                        if (i == j) a += 1 else {
                            b += 1
                        }
                        break
                    }
                }
            }
            return a.toString()+"A"+b+"B"
        }
    }

}