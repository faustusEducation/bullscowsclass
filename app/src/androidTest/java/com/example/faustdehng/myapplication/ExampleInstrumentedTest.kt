package com.example.faustdehng.myapplication

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getTargetContext()
        assertEquals("com.example.faustdehng.myapplication", appContext.packageName)
    }

    @Test
    fun test_checkABs_None() {
        val result = MainActivity.checkABs("1234","5678")
        assertTrue((result=="0A0B"))
    }

    @Test
    fun test_checkABs_Bingo() {
        val result = MainActivity.checkABs("1234","1234")
        assertEquals("4A0B", result)
    }

    @Test
    fun test_checkABs_4B() {
        val result = MainActivity.checkABs("1234","4321")
        assertTrue((result=="0A4B"))
    }

    @Test
    fun test_checkABs_2A2B() {
        val result = MainActivity.checkABs("1432","1234")
        assertTrue((result=="2A2B"))
    }
}
